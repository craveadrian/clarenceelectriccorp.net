<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="hdTop">
				<div class="row">
					<p>FOLLOW US</p>
					<a href="<?php $this->info("fb_link"); ?>" class="socialMed">f</a>
					<a href="<?php $this->info("li_link"); ?>" class="socialMed">i</a>
				</div>
			</div>
			<div class="hdMid">
				<div class="row">
					<div class="hdMidLeft inb">
						<img src="public/images/common/mainLogo1.png" alt="Clarence Electric Corp.">
					</div>
					<div class="hdMidMid inb aln">
						<p>AFTER-HOURS <span>EMERGENCY SERVICE AVAILABLE</span> </p>
					</div>
					<div class="hdMidRight inb aln">
						<img src="public/images/common/phone.png" alt="Phone Icon" class="inb">
						<p class="title inb">CALL US AT</p>
						<p class="phone"><?php $this->info(["phone","tel"]); ?></p>
					</div>
				</div>
			</div>
			<div class="hdBot">
				<div class="row">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">ABOUT US</a></li>
							<li <?php $this->helpers->isActiveMenu("service"); ?>><a href="<?php echo URL ?>service#content">SERVICES</a></li>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
							<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews#content">REVIEWS</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
							<li <?php $this->helpers->isActiveMenu("location"); ?>><a href="<?php echo URL ?>location#content">LOCATION</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<div class="bannerTop">
				<div class="row">
					<p class="widget"> When You Need It Right<span> Now, and Right Now!</span> </p>
				</div>
			</div>
			<div class="bannerBot">
				<div class="row">
					<div class="bnBotLeft">
						<p>If you’ve got an electrical emergency, or are in need of prompt service to your home, don’t hesitate to give us a call or send an e-mail. We’re at you’re service!</p>
					</div>
					<div class="bnBotRight">
						<p>ELECTRICAL HOME IMPROVEMENT SPECIALIST</p>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
