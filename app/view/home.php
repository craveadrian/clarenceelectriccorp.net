<div id="content">
	<div id="welcome-section">
		<div class="row">
			<div class="wlc-panel">
				<h2 >WELCOME TO</h2>
				<h1>Clarence Electric Corporation</h1>
				<div class="wlcLeft inb">
					<h3>YOUR SEASIDE, CA ELECTRICIAN, SPECIALIZING IN:</h3>
					<ul>
						<li>Residential Electrical Repair or Replacement</li>
						<li>Troubleshooting Experts</li>
						<li>Ceiling Fan</li>
						<li>Lighting Installation</li>
						<li>Whole House Surge Protection</li>
						<li>Home Wiring Inspection</li>
					</ul>
				</div>
				<div class="wlcRight inb">
					<img src="public/images/content/img1.jpg" alt="Image Content 1" class="img1">
					<p>When you need quality electricians, turn to Clarence Electric Corporation. For over 35 years our electricians have been providing residential electrical service in Seaside, CA. </p>
					<div class="wlcRightBot">
						<img src="public/images/content/wlcphone.png" alt="Phone Icon">
						<p><?php $this->info(["phone","tel"]); ?></p>
					</div>
					<p class="followUs">FOLLOW US <span><a href="<?php $this->info("fb_link"); ?>" class="socialMed">f</a>
					<a href="<?php $this->info("li_link"); ?>" class="socialMed">i</a><span></p>
				</div>
			</div>
		</div>
	</div>
	<div id="our-service-section">
		<div class="row">
			<div class="svc-panel-images">
				<h2>OUR SERVICES INCLUDED</h2>
				<div class="svc-images">
					<img src="public/images/content/service1.jpg" alt="Service 1">
					<div class="svc-details">
						<p>Repairs or <br/> Replacements</p>
						<a href="<?php echo URL ?>services#content" class="btn">LEARN MORE</a>
					</div>
				</div>
				<div class="svc-images">
					<img src="public/images/content/service2.jpg" alt="Service 2">
					<div class="svc-details">
						<p>Specialty <br/> Lighting</p>
						<a href="<?php echo URL ?>services#content" class="btn2">LEARN MORE</a>
					</div>
				</div>
				<div class="svc-images">
					<img src="public/images/content/service3.jpg" alt="Service 3">
					<div class="svc-details">
						<p>Safety & <br/> Security</p>
						<a href="<?php echo URL ?>services#content" class="btn">LEARN MORE</a>
					</div>
				</div>
				<div class="svc-images">
					<img src="public/images/content/service4.jpg" alt="Service 4">
					<div class="svc-details">
						<p>Electric Energy <br/> Savings</p>
						<a href="<?php echo URL ?>services#content" class="btn2">LEARN MORE</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="service-bottom-section">
		<div class="row">
			<div class="sbsLeft inb">
				<h3>The Clarence Electric Difference</h3>
				<ul>
					<li>Straight forward pricing for electric repair</li>
					<li>Fully-stocked truck; we start and finish when we get there on most calls</li>
					<li>We do all the paperwork when necessary for permits!</li>
					<li>Full time Master Electrician on staff for questions or concerns</li>
					<li>Electrical Troubleshooting EXPERTS!</li>
					<li>Big or small electric repair & installation, inside or outside home!</li>
					<li>New Installation for minor OR major electrical repairs</li>
				</ul>
				<div class="phone">
					<img src="public/images/common/phone.png" alt="Phone Icon">
					<p><?php $this->info(["phone","tel"]); ?></p>
				</div>
			</div>
			<div class="sbsRight inb">
				<div class="viewMore"><img src="public/images/content/servImg1.jpg" alt=""></div>
				<div class="viewMore"><img src="public/images/content/servImg2.jpg" alt=""></div>
				<div class="viewMore"><img src="public/images/content/servImg3.jpg" alt=""></div>
				<div class="viewMore"><img src="public/images/content/servImg4.jpg" alt="">
					<a href="<?php echo URL ?>service#content" class="btn2">VIEW MORE</a>
				</div>
			</div>
		</div>
	</div>
	<div id="contact-section">
		<div class="row">
			<h2>CONTACT US</h2>
			<div class="csLeft inb">
				<img src="public/images/content/contactLogo.png" alt="Clarence Electic Corp. Logo">
				<div class="location ine">
					<div class="contact-info">
						<img src="public/images/common/location.png" alt="">
						<p><?php $this->info("address") ;?></p>
					</div>
					<div class="contact-info">
						<img src="public/images/common/phone.png" alt="">
						<p class="phone"><?php $this->info(["phone","tel"]) ;?></p>
					</div>
					<div class="contact-info">
						<img src="public/images/common/email.png" alt="">
						<p ><?php $this->info(["email","mailto"]) ;?></p>
					</div>
					<div class="contact-info">
						<img src="public/images/common/time.png" alt="">
						<p><?php $this->info("time") ;?></p>
					</div>
				</div>
			</div>
			<div class="csRight inb">
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
						<input type="text" name="name" placeholder="Name:">
						<input type="text" name="phone" placeholder="Phone:">
						<input type="text" name="email" placeholder="Email:">
						<textarea name="message" cols="30" rows="10" placeholder="Message/Questions:"></textarea>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
				</form>
			</div>
		</div>
	</div>
	<div id="accreditation-section">
		<div class="row">
			<h4>Customer Reviews, Accreditation, & Professional Electrical Contractor Organizations:</h4>
			<img src="public/images/content/acc1.png" alt="">
			<img src="public/images/content/acc2.png" alt="">
			<img src="public/images/content/acc3.png" alt="">
			<div class="wing">
				<img src="public/images/content/leftWing.png" alt="">
				<p>Working to Earn Our Wings</p>
				<img src="public/images/content/rightWing.png" alt="">
			</div>
		</div>
	</div>
</div>
